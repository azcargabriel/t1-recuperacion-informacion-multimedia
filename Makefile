# parametros para compilacion
CFLAGS += -std=c++11 -Wall -Wextra

#compilar con optimizaciones
CFLAGS += -O3
#compilar con informacion de debug o compilar con optimizaciones
#CFLAGS += -O0 -ggdb

#usar opencv
CFLAGS  += $(shell pkg-config --cflags opencv)
LDFLAGS += $(shell pkg-config --libs   opencv)

##configurar las rutas de instalacion de opencv
#CFLAGS  += -I[ruta al opencv/include]
#LDFLAGS += -L[ruta al opencv/lib] -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_imgcodecs

#otras librerias que se pueden usar
#LDFLAGS  += -lm -lpthread

########## archivos necesarios para compilar ##########

DETECTION_CPP := $(sort $(wildcard src/detection.cpp))
FEATURES_CPP := $(sort $(wildcard src/features.cpp))
FEATURES_HEADERS := $(sort $(wildcard src/features.hpp))
CLOSER_DISTANCES_CPP := $(sort $(wildcard src/closer_distances.cpp))
CLOSER_DISTANCE_HEADERS := $(sort $(wildcard src/closer_distances.hpp))
UTILS_CPP := $(sort $(wildcard src/utils.cpp))
UTILS_HEADERS := $(sort $(wildcard src/utils.hpp))

########## archivos generados ##########

DETECTION_BIN := $(patsubst src/%,build/%,$(basename $(DETECTION_CPP)))
FEATURES_O := $(patsubst src/%,build/object/%,$(FEATURES_CPP:.cpp=.o))
CLOSER_DISTANCE_O := $(patsubst src/%,build/object/%,$(CLOSER_DISTANCES_CPP:.cpp=.o))
UTILS_O := $(patsubst src/%,build/object/%,$(UTILS_CPP:.cpp=.o))

########## reglas de compilacion ##########

#reglas all y clean no corresponden a archivos
.PHONY: all clean

#no borrar archivos intermedios
.PRECIOUS: build/object/%.o

#por defecto se generan todos los ejecutables de los ejemplos
all: $(DETECTION_BIN)

#para un ejecutable se requiere el object correspondiente mas los utils 
build/detection: build/object/detection.o $(FEATURES_O) $(CLOSER_DISTANCE_O) $(UTILS_O)
	mkdir -p "features"
	g++ $^ -o $@ $(LDFLAGS) $(CFLAGS)

#para generar un object se requiere el fuente correspondiente mas los headers
build/object/%.o: src/%.cpp
	mkdir -p "$(@D)"
	g++ -c $(CFLAGS) -o $@ $<

#limpiar todos los archivos de compilacion
clean:
	rm -rf build/
