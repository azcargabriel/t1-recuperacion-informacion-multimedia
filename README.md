Tarea 1
Recuperacion de informacion multimedia
CC5213-1 2018-1
Gabriel Andres Azocar Carcamo

OBSERVACION
Mi trabajo fue realizado en el sistema operativo macOS, por lo que no se si servira la misma forma de compilacion para linux o windows. Si existen problemas, ruego contactarme.

COMPILACION
En la carpeta 'main', escribir make (por consola). Deberian crearse dos carpetas: build y features.

CONFIGURACION
Hay dos archivos importantes: comerciales.txt y video.txt.
  - comerciales.txt: contiene el path relativo a la carpeta build de todos los comerciales a buscar. En comercialesEjemplo.txt se puede ver un ejemplo del formato.
  - video.txt: Lo mismo que el anterior, pero con el video en el cual buscaremos los comerciales. En videoEjemplo.txt se puede ver un ejemplo del formato.

EJECUCION
Entrar a la carpeta build y ejecutar './detection param1 param2' donde los parametros son:
param1 -> 0 si se quiere hacer el proceso entero, 1 si se quiere partir desde el calculo de distancias y 2 si se quiere partir del analisis del calculo de distancias.
param2 -> Solo necesario si param1 = 1. Debe contener la cantidad de comerciales que vamos a buscar.
Por ejemplo: Si ya tengo los archivos de caracteristicas de mis videos, solo debo hacer './detection 1 21', suponiendo que tengo 21 comerciales a buscar. Por otra parte, si ya tengo el archivo generado por el proceso 1, solo puedo poner './detection 2'

RESULTADOS
El sistema entero se demora aproximadamente 2 minutos y 15 segundos.
Las tasas de deteccion (usando el script subido por el profesor) entre los videos de prueba fueron:
mega-2014_04_10 -> 84.6%
mega-2014_04_11 -> 92.9%
mega-2014_04_16 -> 80%
mega-2014_04_17 -> 81.8%
mega-2014_04_23 -> 87.5%
mega-2014_04_25 -> 80%