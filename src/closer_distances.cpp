#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <fstream>
#include <string.h>

#include "closer_distances.hpp"

std::vector<std::vector<int> > video;

//Vector de minimos por frame, siempre ordenado de menor a mayor.
//Cada vector es de 3 elementos.
std::vector<std::vector<DistanceInfo> > mins;
int q;

//Inserta d en v si es que es menor que el mayor de los elementos que hay.
bool insertMins(std::vector<DistanceInfo>& v, DistanceInfo d) {
  if(v.empty() || v.size() < 3) {
    v.push_back(d);
    std::sort(v.begin(), v.end());
    return true;
  }
  else {
    if(d < v.back()) {
      v.pop_back();
      v.push_back(d);
      std::sort(v.begin(), v.end());
      return true;
    }
  }

  return false;
}

//Distancia euclideana multidimensional.
int distance(std::vector<int> v1, std::vector<int> v2) {
  long sum = 0;

  if(v1.size() != v2.size()) {
    std::cout << "Vectores de distinta dimension (" 
    << v1.size() << " y " << v2.size() << ")" << std::endl;
    return sum;
  }

  for(int i = 0; i < (int)v1.size(); i++) {
    int r = v1[i] - v2[i];
    sum += r * r;
  }

  return (int) std::sqrt(sum);
}

int countLines(std::string name) {
    int count = 0;
    std::string line;
 
    /* Creating input filestream */ 
    std::ifstream file(name);
    while (getline(file, line))
        count++;

    return count;
}

void getMainVideo() {
  //Guardamos el video principal (donde se buscaran los comerciales) en la variable video.
  std::ifstream infile("../features/0");
  if(infile.fail()) {
    std::cout << "ERROR reading file 0" << std::endl;
    return;
  }

  std::string line;
  std::vector<int> frame;

  int i = 0;
  while(std::getline(infile, line, ' ')) {
    frame.push_back(std::atoi(line.c_str()));
    i++;
    if(i % 100 == 0) {
      video.push_back(frame);
      frame.clear();
      i = 0;
    }
  }

  //Inicializamos mins con el tamano que necesitamos.
  std::vector<std::vector<DistanceInfo> > m(video.size());
  mins = m;
}

void calculateDistances(int i) {
  std::vector<std::vector<int> > actualAd;

  std::string line;
  std::vector<int> frame;

  DistanceInfo dist(i, 0, 0);

  //Obtenemos un comercial, y lo guardamos en actualAd.
  std::string fileName = std::to_string(i);
  std::ifstream infile("../features/" + fileName);
  if(infile.fail()) {
    std::cout << "ERROR reading file " << fileName << std::endl;
    return;
  }

  int j = 0;
  while(std::getline(infile, line, ' ')) {
    frame.push_back(std::atoi(line.c_str()));
    j++;
    if(j % 100 == 0) {
      actualAd.push_back(frame);
      frame.clear();
      j = 0;
    }
  }

  //Comparamos el comercial recien cargado con el video.
  for(int z = 0; z < (int)video.size(); z++) {
    for(int k = 0; k < (int)actualAd.size(); k++) {
      dist.setFrame(k);
      dist.setDistance(distance(video[z], actualAd[k]));
      //Hacemos que insertMins lo inserte como minimo si cumple el criterio.
      insertMins(mins[z], dist);
    }
  }

  std::cout << "Lista busqueda de comercial " << i << " de " << q << std::endl;
  actualAd.clear();
}

void writeResults() { 
  //Creamos un archivo para guardar la salida.
  std::ofstream myfile;
  myfile.open("../features/distances.txt");
  for(int i = 0; i < (int)mins.size(); i++) {
    for(int j = 0; j < (int)mins[i].size(); j++) {
      myfile << mins[i][j].getFileName() << ";" << mins[i][j].getFrame() << ";" << mins[i][j].getDistance() << ' ';
    }
    myfile << std::endl;
  }
}

void calculate(int quantity) {
  q = quantity;

  std::cout << "Buscando..." << std::endl;
  getMainVideo();

  //Buscamos el frame mas parecido al frame del video entre todos los comerciales.
  for(int i = 1; i <= quantity; i++) {
   calculateDistances(i);
  }

  writeResults();
}
