#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <fstream>
#include <string.h>

#include "features.hpp"
#include "closer_distances.hpp"
#include "utils.hpp"

#define SKIP_THRESHOLD 4

//Las respuestas de apariciones.
std::vector<AppareanceInfo> answers;

//Nombres de los archivos.
std::string videoName;
std::vector<std::string> advertisements;

//De un string de la forma x;y;z generamos un DistanceInfo
//de la forma DistanceInfo(x, y, z) con x, y, z int.
DistanceInfo getDistancePairFromString(std::string str) {
  char *token;
  std::string s = ";";
  std::vector<int> values;
  token = strtok((char*)str.c_str(), (char*)s.c_str());
  values.push_back(std::atoi(token));

  while((token = strtok(NULL, (char*)s.c_str())) != NULL) {
    values.push_back(std::atoi(token));
  }

  return DistanceInfo(values[0], values[1], values[2]);
}

//De un string de la forma "x1;y1;z1 x2;y2;z2 x3;y3;z3" generamos un vector 
//de tres DistanceInfos.
std::vector<DistanceInfo> getDistancePairs(std::string str) {
  char *token;
  std::string s = " ";
  std::vector<std::string> values;
  std::vector<DistanceInfo> infos;

  token = strtok((char*)str.c_str(), (char*)s.c_str());
  values.push_back(token);

  while((token = strtok(NULL, (char*)s.c_str())) != NULL) {
    values.push_back(token);
  }

  for(int i = 0; i < (int)values.size(); i++) {
    infos.push_back(getDistancePairFromString(values[i]));
  }

  return infos;
}

//Extrae el nombre de un archivo ignorando el path completo
std::string splitName(std::string str) {
  char *token;
  std::string s = "/";
  std::vector<std::string> values;

  token = strtok((char*)str.c_str(), (char*)s.c_str());
  values.push_back(token);

  while((token = strtok(NULL, (char*)s.c_str())) != NULL) {
    values.push_back(token);
  }

  return values.back();
}

//Obtenemos los nombres de todos los comerciales y el video donde los buscaremos.
void getNames() {
  std::ifstream infile1("../video.txt");
  if(infile1.fail()) {
    std::cout << "ERROR reading file ../video.txt" << std::endl;
    return;
  }
  std::getline(infile1, videoName);

  std::string line;
  std::ifstream infile2("../comerciales.txt");
  if(infile2.fail()) {
    std::cout << "ERROR reading file ../comerciales.txt" << std::endl;
    return;
  }

  while(std::getline(infile2, line)) {
    advertisements.push_back(line);
  }
}

void searchAnswers() {
  std::ifstream infile("../features/distances.txt");
  if(infile.fail()) {
    std::cout << "ERROR reading file ../distances.txt" << std::endl;
    return;
  }

  std::string line;
  DistanceInfo candidate(-1, -1, -1);

  int startFrame = 0;
  int consecutiveFrames = 0;
  long currentFrame = 0;

  while(std::getline(infile, line)) {
    currentFrame++;
    std::vector<DistanceInfo> infos = getDistancePairs(line);
    //Si no existe candidato
    if(candidate.getDistance() == -1) {
      //Vemos si alguno de los info cumplen con las condiciones para
      //ser elegido candidato
      for(int i = 0; i < (int)infos.size(); i++) {
        if(infos[i].getFrame() < 10) {
          if(infos[i].getDistance() < 300) {
            candidate = infos[i];
            startFrame = currentFrame;
            break;
          }
        }
      }
    }

    if(candidate.getDistance() == -1) continue;

    //Si ya tenemos un candidato, tenemos dos opciones: 
    //que el candidato siga avanzando o sea descartado.
    //Revisamos los siguientes frames.
    int skippedFrames = 0;
    while(std::getline(infile, line)) {
      currentFrame++;
      bool valid = false;
      std::vector<DistanceInfo> nextInfos = getDistancePairs(line);
      //Vemos si alguno tiene parecido con algun frame 
      //siguiente (y cercano) del comercial.
      for(int j = 0; j < (int)nextInfos.size(); j++) {
        //Si son del mismo comercial, revisamos frame y distancia.
        if(candidate.getFileName() == nextInfos[j].getFileName()) {
          if(candidate.getFrame() <= nextInfos[j].getFrame() 
            && nextInfos[j].getFrame() <= candidate.getFrame() + 3) {
            if(nextInfos[j].getDistance() < 300) {
              consecutiveFrames++;
              candidate = nextInfos[j];
              valid = true;
              break;
            }
          }
        }
      }

      if(!valid) {
        if(++skippedFrames < SKIP_THRESHOLD) continue;

        //Revisamos si el candidato avanzo lo suficiente para convertirse en aparicion.
        if(FRAMES_PER_SECOND * 5 <= consecutiveFrames) {
          answers.push_back(AppareanceInfo(candidate.getFileName(), startFrame, currentFrame));
        }
        //Luego lo borramos.
        candidate.setDistance(-1);
        consecutiveFrames = 0;
        skippedFrames = 0;
        break;
      }
    }
  }
}

void writeAnswers() {
  //Creamos un archivo para guardar la salida.
  std::ofstream myfile;
  myfile.open("../answers.txt");

  for(std::vector<AppareanceInfo>::iterator it = answers.begin(); it != answers.end(); ++it) {
    myfile << splitName(videoName) <<"\t"
    << it->getStartSecond()/float(FRAMES_PER_SECOND) << "\t" 
    << ((it->getFinishSecond() - it->getStartSecond())/float(FRAMES_PER_SECOND)) << "\t"
    << splitName(advertisements[it->getFileName() - 1]) << std::endl;
  }
}

int main(int argc, char* argv[]) {

  if(argc < 2 || (std::atoi(argv[1]) == 1  && argc != 3)) {
    std::cout << "Error al entregar parametros, por favor leer README.md" << std::endl;
    exit(1);
  }

  int num;
  if(argc == 3) {
    num = std::atoi(argv[2]);
  }

  switch(std::atoi(argv[1])) {

  case 0:
    std::cout << "Extrayendo caracteristicas, puede demorar un par de minutos..." << std::endl;
    num = extract();

  case 1:
    std::cout << "Calculando distancias..." << std::endl;
    calculate(num);

  case 2:
    //Cargamos los nombres de los comerciales y el video en que los buscaremos.
    //Esto servira para cuando necesite escribir mis resultados.
    getNames();

    //Buscaremos comerciales a traves de los frames consecutivos.
    //Buscaremos un candidato a comercial, que tiene que cumplir lo siguiente
    //Su primera aparicion tiene que ser con uno de sus primeros frames
    //y la distancia no debe superar los 300 (obtenido empiricamente).
    std::cout << "Buscando apariciones..." << std::endl;
    searchAnswers();

    //Escribimos el archivo de respuestas.
    writeAnswers();

  default:
    std::cout << "Listo! La respuesta esta en answers.txt" << std::endl;
    return 0;

  }

  return 0;
}

