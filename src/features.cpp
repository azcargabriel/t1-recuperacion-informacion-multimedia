#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>
#include <fstream>
#include <string.h>

#include "features.hpp"

/*
  name corresponde al nombre del archivo donde estan los nombres
  de los videos a los cuales le vamos a extraer las caracteristicas.
  i corresponde a un numero, para el nombre del archivo.
  Por ejemplo, si i = 0, el archivo que contiene las 
  caracteristicas del primer video se llamara 0, el del segundo 1,
  tercero 2, etc.
*/
int extractFeatures(std::string name, int ind) {
  //Cantidad de videos analizados
  int total = 0;

  //Variables de opencv para manipular los videos.
  cv::VideoCapture video;
  cv::Mat frame, resizeFrame, grayScaleFrame;

  //Obtenemos los nombres de los videos a buscar.
  std::ifstream infile(name);
  std::string line;
  std::vector<std::string> names;
  while(std::getline(infile, line)) {
    names.push_back(line);
  }

  for(std::vector<std::string>::iterator it = names.begin(); it != names.end(); ++it) {
    video.open(*it);

    if(!video.isOpened()) {
      std::cout << "\nERROR al abrir el archivo" << std::endl << std::endl;
      return total;
    }

    //Creamos un archivo para guardar las caracteristicas del video
    //sera de texto plano, no es lo mas eficiente, pero es lo mas facil
    //de manejar. Una mejora seria hacerlo con binarios.
    std::ofstream myfile;
    myfile.open("../features/" + std::to_string(ind));

    //Por experimentacion obtuve que los videos estan a 30 fps, por lo que para
    //obtener 3 frames por segundo debo tomar 1 cada 10 frames,
    //para obtener 6 frames por segundo debo tomar 1 cada 5, etc.
    //Defino FRAMES_PER_SECOND para decidir cuantos tomo.

    //Tomamos el primer frame y lo guardamos.
    int frames = 1;
    video.read(frame);
    cv::resize(frame, resizeFrame, cv::Size(10, 10));
    cv::cvtColor(resizeFrame, grayScaleFrame, cv::COLOR_BGR2GRAY);

    for(int j = 0; j < grayScaleFrame.cols; j++) {
      for(int i = 0; i < grayScaleFrame.rows; i++) {
        myfile << (int)grayScaleFrame.at<uchar>(j, i, 0) << ' ';
      }
    }
    myfile << std::endl;

    while(video.isOpened()) {
      //Si el video sigue.
      if ((video.get(CV_CAP_PROP_POS_FRAMES) + 1) < video.get(CV_CAP_PROP_FRAME_COUNT)) {
        video.read(frame);
        frames++;

        //Si es un frame divisible por 30 / FRAMES_PER_SECOND, lo transformamos y guardamos.
        if(frames % (30 / FRAMES_PER_SECOND) == 0) {
          cv::resize(frame, resizeFrame, cv::Size(10, 10));
          cv::cvtColor(resizeFrame, grayScaleFrame, cv::COLOR_BGR2GRAY);
          for(int j = 0; j < grayScaleFrame.cols; j++) {
            for(int i = 0; i < grayScaleFrame.rows; i++) {
              myfile << (int)grayScaleFrame.at<uchar>(j, i, 0) << ' ';
            }
          }
          myfile << std::endl;
          frames = 0;
        }
      }
      else {
          break;
      }
    }

    myfile.close();

    ind++;
    total++;
    std::cout << "Caracteristicas de "<< *it << " guardadas" << std::endl;
  }

  return total;
}

int extract() {
  //Extraemos las caracteristicas del video a buscar.
  extractFeatures("../video.txt", 0);

  //Extraermos las caracteristicas de los comerciales a buscar.
  return extractFeatures("../comerciales.txt", 1);
}
