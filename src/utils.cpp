#include "utils.hpp"

//Distance info es una clase que identifica completamente una distancia entre un frame y
//el video principal. fileName es el nombre del comercial de donde proviene,
//frame el frame al cual pertenece la distancia que se guarda en distance.
DistanceInfo::DistanceInfo(int fileName, int frame, int distance) {
  this->fileName = fileName;
  this->frame = frame;
  this->distance = distance;
}

int DistanceInfo::getFileName() {
  return this->fileName;
}

int DistanceInfo::getDistance() {
  return this->distance;
}

int DistanceInfo::getFrame() {
  return this->frame;
}

void DistanceInfo::setDistance(int d) {
  this->distance = d;
}

void DistanceInfo::setFrame(int f) {
  this->frame = f;
}

void DistanceInfo::clear() {
  this->fileName = -1;
  this->frame = -1;
  this->distance = -1;
}

bool DistanceInfo::operator < (const DistanceInfo& d) const {
  return this->distance < d.distance;
}

bool DistanceInfo::operator > (const DistanceInfo& d) const {
  return this->distance > d.distance;
}

//Clase que identifica una aparicion. Contiene el nombre del comercial, segundo de comienzo y final.
AppareanceInfo::AppareanceInfo(int fileName, int startSecond, int finishSecond) {
  this->fileName = fileName;
  this->startSecond = startSecond;
  this->finishSecond = finishSecond;
}

int AppareanceInfo::getFileName() {
  return this->fileName;
}

int AppareanceInfo::getStartSecond() {
  return this->startSecond;
}

int AppareanceInfo::getFinishSecond() {
  return this->finishSecond;
}