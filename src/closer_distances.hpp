#include "utils.hpp"

bool insertMins(std::vector<DistanceInfo>& v, DistanceInfo d);

int distance(std::vector<int> v1, std::vector<int> v2);

int countLines(std::string name);

void getMainVideo();

void calculateDistances(int quantity);

void writeResults();

void calculate(int quantity);
