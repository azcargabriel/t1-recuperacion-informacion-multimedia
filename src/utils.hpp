#pragma once

class DistanceInfo {
  int fileName;
  int frame;
  int distance;

  public:
    DistanceInfo(int fileName, int frame, int distance);
    int getFileName();
    int getDistance();
    int getFrame();
    void setDistance(int d);
    void setFrame(int f);
    void clear();
    bool operator < (const DistanceInfo& d) const;
    bool operator > (const DistanceInfo& d) const;
};


class AppareanceInfo {
  int fileName;
  int startSecond;
  int finishSecond;
public:
  AppareanceInfo(int fileName, int startSecond, int finishSecond);
  int getFileName();
  int getStartSecond();
  int getFinishSecond(); 
};